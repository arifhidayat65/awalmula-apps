# Project Title

Awal Mula App

### Installation

1. Clone Repo

```bash
git clone -b master https://github.com/anieswindi/awalmula-app.git
```

2. Use the node package manager to install this.

```
npm install
```

3. React start scripts

```
npm start
```

### Usage

1. Make sure your `LocalStorage` has been clear.

2. You can insert anything for username and password in this form (required).

3. I using dummy data when API call error.

4. In this Application, these are work flow :

    - You can add stuff to cart.
    - You can update cart (add new data cart).
    - You can `LOGOUT` your account when you click LOGOUT button in Account Settings.
    - You can't order (I'm not activated `ORDER` button, just check total price when you click stuff selected).
    - Search filter in `HOME` page not active.
    

### Deployment

-   Stagging deployment has moved here with `Heroku`: [https://awalmula-app.herokuapp.com/login].

-   Please reload page if you get `Application Error` there.

## Authors & Developer

-   **Arif Hidayat** - _Initial work_ - [Gitlab](https://gitlab.com/arifhidayat65/awalmula-apps)
-   **LOGIN username : Admin || Password : Admin**