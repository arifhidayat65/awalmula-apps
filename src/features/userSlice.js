const user = JSON.parse(localStorage.getItem("user"));

const initialState = user
	? {
			id: user.id,
			username: user.username,
			password: user.password,
			address_list: [
				{
					id: 1,
					address: "Jalan Saco No.90 Ragunan Jakarta Selatan",
				},
			],
			cart_list: [],
	  }
	: {
			id: null,
			username: null,
			password: null,
			address_list: null,
			cart_list: null,
	  };

function setLocalStorage(user) {
	localStorage.setItem("user", JSON.stringify(user));
}
const users = (state = initialState, action) => {
	switch (action.type) {
		case "POST_USER":
			setLocalStorage(action.payload);
			return {
				id: 1,
				username: action.payload.username,
				password: action.payload.password,
				address_list: [
					{
						id: 1,
						address: "Jalan Saco No.90 Ragunan Jakarta Selatan",
					},
				],
				cart_list: [],
			};
		case "DELETE_USER":
			localStorage.removeItem("user");
			return state;
		case "GET_TOKEN":
			return state;
		case "ADD_CART":
			return {
				...state,
				cart_list: [action.payload, ...state.cart_list],
			};
		default:
			return state;
	}
};

export default users;
