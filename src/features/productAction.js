import axios from "axios";
import store from "../Store";

//param location still dummy//
export const getProduct = (param) => {
	return (dispatch) => {
		axios
			.get("https://staging.awalmula.co.id/rest/default/V1/products", {
				params: param,
				headers: {
					"Content-Type": "application/xml",
				},
			})
			.then((res) => {
				dispatch({
					type: "GET_PRODUCT",
					payload: res.data.items,
				});
			})
			.catch((err) => {
				dispatch({
					type: "ERROR",
					payload: "",
				});
			});
	};
};

export const updateSearch = (payload) => {
	store.dispatch({ type: "UPDATE_SEARCH", payload: payload });
};
