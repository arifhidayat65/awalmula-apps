import store from "../Store";

function setUser(data) {
	store.dispatch({ type: "POST_USER", payload: data });
}

function logout() {
	localStorage.removeItem("user");
	store.dispatch({ type: "DELETE_USER", payload: "Learn about actions" });
}

function addStuffToChartList(stuff) {
	store.dispatch({ type: "ADD_CART", payload: stuff });
}

export { setUser, logout, addStuffToChartList };
