const initialState = {
	product: [],
	search: "",
};

const products = (state = initialState, action) => {
	switch (action.type) {
		case "GET_PRODUCT":
			return {
				product: action.payload,
			};
		case "ERROR":
			return {
				product: [
					{
						id: 1,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 10500,
					},
					{
						id: 2,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 3,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 4,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 5,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 6,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 7,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 8,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 9,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 10,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 11,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 12,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
					{
						id: 13,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 14,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 15,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 16,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 17,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 18,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 19,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: false,
						diskonPrice: null,
					},
					{
						id: 20,
						name: "Monstera deliciosa variegata monvar",
						type : "ROOM DECORATION",
						description:
							"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
						price: 13700,
						total_stuff: 15,
						identity_shop: {
							shop_name: "Rora BlessingShop",
							shop_address: "Kota Bogor",
							id: 1334,
						},
						isDiskon: true,
						diskonPrice: 6000,
					},
				],
			};

		case "UPDATE_SEARCH":
			return {
				...state,
				search: action.payload,
			};
		default:
			return state;
	}
};

export default products;
