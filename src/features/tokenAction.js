import axios from "axios";

//param location still dummy//
export const getToken = () => {
	const data = {
		username: "admin",
		password: "admin123",
	};
	return (dispatch) => {
		axios
			.post(
				"https://staging.awalmula.co.id/rest/V1/integration/admin/token",
				data,
				{
					headers: {
						"Content-Type": "application/xml",
					},
				}
			)
			.then((res) => {
                console.log(res)
				dispatch({
					type: "GET_TOKEN",
					payload: res.data.items,
				});
			})
			.catch((err) => console.log(err));
	};
};
