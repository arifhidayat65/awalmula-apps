import React, { useEffect } from "react";
import Login from "./pages/Login/Login";
import Home from "./pages/Home/Home";
import Cart from "./pages/Cart/Cart";
import Account from "./pages/Account/Account";
import ProductDetail from "./pages/ProductDetail/ProductDetail";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from "./route/privateRoute";
import { useDispatch } from "react-redux";
import { getToken } from "./features/tokenAction";

function App(props) {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(getToken());
	});
	return (
		<div className="Dashboard">
			<Router>
				<Switch>
					<Route
						exact
						path="/product-details/:id"
						component={ProductDetail}
					/>
					<Route exact path="/account" component={Account} />
					<Route exact path="/cart" component={Cart} />
					<Route exact path="/login" component={Login} />
					<PrivateRoute exact path="/" component={Home} />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
