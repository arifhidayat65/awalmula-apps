import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import Footer from "../../components/Footer";
import avatar from "../../assets/agnesmonica.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faCreditCard,
	faMoneyBillWave,
	faDatabase,
	faLock,
	faBell,
	faIdCard,
	faListAlt,
	faTags,
} from "@fortawesome/free-solid-svg-icons";
import * as users from "../../features/userAction";

import "./Account.css";

function Account(props) {
	const isDesktopOrLaptop = useMediaQuery({
		query: "(min-device-width: 1224px)",
	});
	const isTablet = useMediaQuery({ query: "(max-width: 1224px)" });
	const isMobileDevice = useMediaQuery({
		query: "(max-device-width: 414px)",
	});

	const [settings] = useState([
		{
			id: 1,
			content: {
				title: "Address List",
				sub_title: "Set shopping delivery address",
			},
			icon: faCreditCard,
		},
		{
			id: 2,
			content: {
				title: "Bank Account",
				sub_title: "Withdraw balance to destination account",
			},
			icon: faDatabase,
		},
		{
			id: 3,
			content: {
				title: "Instant Payment",
				sub_title: "Ovo, credit, debit",
			},
			icon: faCreditCard,
		},
		{
			id: 4,
			content: {
				title: "Account Security",
				sub_title: "Set password, verification, and PIN",
			},
			icon: faLock,
		},
		{
			id: 5,
			content: {
				title: "Notifications",
				sub_title: "Set any kind of notification message",
			},
			icon: faBell,
		},
	]);

	const members = [
		{
			id: 1,
			icon: faIdCard,
			contents: {
				title: "Member Shopping",
				total: "0 Member",
			},
		},
		{
			id: 2,
			icon: faListAlt,
			contents: {
				title: "Top Quest",
				total: "11 Question",
			},
		},
		{
			id: 3,
			icon: faTags,
			contents: {
				title: "My Cuppon",
				total: "27 Cuppon",
			},
		},
	];

	const logout = () => {
		users.logout();
		window.location.assign("/login");
	};

	if (isMobileDevice) {
		return (
			<div className="Pages">
				<div className="Account">
					<div className="_acontent">
						<div className="_avatar">
							<div className="_ava">
								<img src={avatar} alt="avatar" />
							</div>
							<div className="_info">
								<span>Agnes Monica</span>
								<span
									style={{
										fontWeight: "normal",
										fontSize: "14px",
									}}
								>
									+628281727272
								</span>
							</div>
						</div>
						<div className="_card">
							<div className="_emoney">
								<div className="_icons">
									<FontAwesomeIcon icon={faCreditCard} />
								</div>
								<div className="_total">
									<span>Rp 5.200</span>
									<span>(52 Points)</span>
								</div>
							</div>
							<div className="_emoney">
								<div className="_icons2">
									<FontAwesomeIcon icon={faMoneyBillWave} />
								</div>
								<div className="_total">
									<span>Rp 0</span>
									<span>Saldo</span>
								</div>
							</div>
						</div>
						<div className="_member">
							<span className="">Member Gold</span>
							<div className="_membercard_">
								{members.map((member, i) => {
									return (
										<div
											className="__Items"
											key={`member-${i}`}
										>
											<div className="ItemContents">
												<FontAwesomeIcon
													icon={member.icon}
												/>
												<span
													style={{
														fontWeight: "bold",
													}}
												>
													{member.contents.title}
												</span>
												<span
													style={{
														fontWeight: "normal",
													}}
												>
													{member.contents.total}
												</span>
											</div>
										</div>
									);
								})}
							</div>
						</div>
					</div>
					<div className="_asetting">
						<div className="_bottom">
							<span className="_titleSett">Account Settings</span>
							{settings.map((setting, i) => {
								return (
									<div
										className="_settItem"
										key={`setting-${i}`}
									>
										<div className="_imgs">
											<FontAwesomeIcon
												icon={setting.icon}
											/>
										</div>
										<div className="_contents">
											<span
												style={{ fontWeight: "bold" }}
											>
												{setting.content.title}
											</span>
											<span>
												{setting.content.sub_title}
											</span>
										</div>
									</div>
								);
							})}
						</div>
					</div>
					<div className="logout">
						<button onClick={logout}>LOGOUT</button>{" "}
					</div>
				</div>
				<Footer></Footer>
			</div>
		);
	} else if (isDesktopOrLaptop) {
		return <></>;
	} else if (isTablet) {
		return <></>;
	}
}

export default Account;
