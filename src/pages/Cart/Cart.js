import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import Footer from "../../components/Footer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import shop from "../../assets/plan_2.jpg";
import store from "../../Store";
import {
	faMapMarkerAlt,
	faCheckSquare,
	faTrash,
	faPlusCircle,
	faMinusCircle,
} from "@fortawesome/free-solid-svg-icons";
import "./Cart.css";

function Cart(props) {
	const [total, setTotal] = useState(0);

	const { users } = store.getState();
	const isDesktopOrLaptop = useMediaQuery({
		query: "(min-device-width: 1224px)",
	});
	const isTablet = useMediaQuery({ query: "(max-width: 1224px)" });
	const isMobileDevice = useMediaQuery({
		query: "(max-device-width: 414px)",
	});

	const numberWithCommas = (x) => {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	};

	const updateCheckbox = (i) => {
		let total, priceProduct, resultPrice;
		const newCart = [...users.cart_list];
		total = newCart[i].totalOrder;
		priceProduct = !newCart[i].stuff.diskonPrice
			? newCart[i].stuff.price
			: newCart[i].stuff.diskonPrice;
		resultPrice = total * priceProduct;
		setTotal(resultPrice);
	};

	if (isMobileDevice) {
		return (
			<div className="Pages">
				<div className="Cart">
					<div className="__carts">
						<div className="__SelectAll"></div>
						<div className="__MemberInfo">
							<span>
								Hi Members, your Free Shipping quota is 3x (for
								1 order/transaction) for this week.
							</span>
						</div>
						<div className="__Sending">
							<FontAwesomeIcon icon={faMapMarkerAlt} />
							<span>
								{users.address_list &&
									users.address_list[0].address}
							</span>
						</div>
						{users.cart_list.length > 0 ? (
							users.cart_list.map((cart, i) => {
								return (
									<div className="__Cart" key={`cart-${i}`}>
										<div className="Shop">
											<div className="Identity">
												<div className="Names">
													<FontAwesomeIcon
														icon={faCheckSquare}
													/>
													<span>
														{
															cart.stuff
																.identity_shop
																.shop_name
														}
													</span>
												</div>
												<div className="City">
													<span>
														{
															cart.stuff
																.identity_shop
																.shop_addressf
														}
													</span>
												</div>
											</div>
											<div className="Stuff">
												<input
													type="checkbox"
													id={`checkbox-${i}`}
													onClick={() =>
														updateCheckbox(i)
													}
												/>
												<div className="Imgss">
													<img
														src={shop}
														alt="shop"
													/>
												</div>
												<div className="Contentss">
													<span className="Titles">
														{cart.stuff.name}
													</span>
													{cart.stuff.isDiskon ? (
														<span className="Price">
															Rp{" "}
															{numberWithCommas(
																cart.stuff
																	.diskonPrice
															)}
														</span>
													) : (
														<span className="Price">
															Rp{" "}
															{numberWithCommas(
																cart.stuff.price
															)}
														</span>
													)}
													<div className="Order">
														<FontAwesomeIcon
															icon={faTrash}
														/>
														<FontAwesomeIcon
															icon={faMinusCircle}
														/>
														<input
															type="text"
															value={
																cart.totalOrder
															}
														/>
														<FontAwesomeIcon
															icon={faPlusCircle}
														/>
													</div>
												</div>
											</div>
											<div className="Totalstuff"></div>
										</div>
									</div>
								);
							})
						) : (
							<div className="NoOrder">
								<span>No cart order</span>
							</div>
						)}
						<div className="TotalOrderPrice">
							<div className="__left">
								<span className="__totalPrice">
									Total :{" "}
									<b>Rp &nbsp;{numberWithCommas(total)}</b>
								</span>
							</div>
							<div className="__right">
								<button>Checkout</button>
							</div>
						</div>
					</div>
				</div>
				<Footer></Footer>
			</div>
		);
	} else if (isDesktopOrLaptop) {
		return <></>;
	} else if (isTablet) {
		return <></>;
	}
}

export default Cart;
