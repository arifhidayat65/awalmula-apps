/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import Footer from "../../components/Footer";
import { getProduct } from "../../features/productAction";
import plants from "../../assets/plan.jpg";
import plants2 from "../../assets/plan_2.jpg";
import plants3 from "../../assets/plan_3.jpg";
import AwesomeSlider from "react-awesome-slider";
import { Link } from "react-router-dom";

import "./Mobile.css";
import "react-awesome-slider/dist/styles.css";
import withAutoplay from "react-awesome-slider/dist/autoplay";

const mapStateToProps = (state) => {
	return { products: state.products.product, search: state.products.search };
};

const AutoplaySlider = withAutoplay(AwesomeSlider);

class Mobile extends Component {
	componentDidMount() {
		const param = {
			"searchCriteria[pageSize]": 20,
		};
		this.props.dispatch(getProduct(param));
	}

	numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	renderContent() {
		const item = [];

		for (let i = 0; i < 10; i++) {
			item.push(
				<div className="__img__" key={`img-index-${i}`}>
					<img src={plants} alt="img" />
					<div className="__content2">
						<h2>Herbal Design Classic Plan</h2>
						<span>
							Cactus originates from the Greek name Kaktos,
							Spanish name “prickly plant of Sicily”.
						</span>
						<span
							style={{
								marginTop: "1rem",
								fontWeight: "bold",
								color: "#4a4a4a",
							}}
						>
							Rp {this.numberWithCommas(13000)}
						</span>
					</div>
				</div>
			);
		}

		return item;
	}

	renderDummy() {
		const dummy = [];
		for (let i = 0; i < 20; i++) {
			dummy.push(
				<div className="ItemData" key={`dummy-${i}`}>
					<div className="ImgItem">
						<div className="__img">
							<img
								src={i % 2 === 0 ? plants2 : plants3}
								alt="image"
							/>
						</div>
						<div className="__content">
							<h2>Null</h2>
							<span>Rp {this.numberWithCommas(100000)}</span>
						</div>
					</div>
				</div>
			);
		}

		return dummy;
	}

	handleChange(e) {}
	render() {
		return (
			<div className="Pages">
				<div className="Main">
					<div className="Top"></div>
					<div className="Bottom"></div>
				</div>
				<div className="Overlay">
					<div className="A">
						<div className="B">
							<h2 className="TextTitle">Discovery</h2>
							<div className="__child1">
								<input
									type="text"
									placeholder="What are you looking for?"
									value={this.props.search}
									onChange={(e) =>
										this.handleChange(e.target.value)
									}
								/>
							</div>
							<h2 className="TextTitle">Hot Product</h2>
							<div className="__child2">
								<AutoplaySlider
									play={true}
									cancelOnInteraction={false}
									interval={3000}
									className="__awesome"
									animation="cubeAnimation"
								>
									{this.renderContent()}
								</AutoplaySlider>
							</div>
						</div>
						<div className="C">
							<h1>New Product</h1>
							<div className="Card">
								{this.props.products.length > 0
									? this.props.products.map((pro, i) => {
											return (
												<div
													className="ItemData"
													key={`product-${i}`}
												>
													<div className="ImgItem">
														<div className="__img">
															<img
																src={
																	i % 2 === 0
																		? plants2
																		: plants3
																}
																alt={`image${i}`}
															/>
														</div>
														<Link
															to={{
																pathname: `/product-details/${pro.id}`,
																state: pro,
															}}
														>
															<div className="__content">
																<h2>
																	{pro.name}
																</h2>
																{pro.isDiskon ? (
																	pro.isDiskon ? (
																		<span
																			style={{
																				fontWeight:
																					"bold",
																			}}
																		>
																			Rp{" "}
																			{this.numberWithCommas(
																				pro.diskonPrice
																			)}
																		</span>
																	) : (
																		<span>
																			Rp{" "}
																			{this.numberWithCommas(
																				pro.price
																			)}
																		</span>
																	)
																) : (
																	<></>
																)}
																<span
																	style={{
																		textDecoration:
																			pro.isDiskon
																				? pro.isDiskon
																					? "line-through"
																					: "unset"
																				: "unset",
																		color: pro.isDiskon
																			? pro.isDiskon
																				? "red"
																				: "black"
																			: "",
																	}}
																>
																	Rp{" "}
																	{this.numberWithCommas(
																		pro.price
																	)}
																</span>
															</div>
														</Link>
													</div>
												</div>
											);
									  })
									: this.renderDummy()}
							</div>
						</div>
					</div>
					<Footer></Footer>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps, null)(Mobile);
