import React from "react";
import { useMediaQuery } from "react-responsive";
import Mobile from "./Mobile";

function Home(props) {
	const isDesktopOrLaptop = useMediaQuery({
		query: "(min-device-width: 1224px)",
	});
	const isTablet = useMediaQuery({ query: "(max-width: 1224px)" });
	const isMobileDevice = useMediaQuery({
		query: "(max-device-width: 414px)",
	});

	if (isMobileDevice) {
		return <Mobile />;
	} else if (isDesktopOrLaptop) {
		return <></>;
	} else if (isTablet) {
		return <></>;
	}
}

export default Home;
