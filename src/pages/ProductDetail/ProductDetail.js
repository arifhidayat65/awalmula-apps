/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import Footer from "../../components/Footer";
import example from "../../assets/plan_2.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import "./ProductDetail.css";
import swal from 'sweetalert';
import * as users from "../../features/userAction";

function ProductDetail(props) {
	const [order, setOrder] = useState(0);
	let price, name, desc, priceDiskon, type;
	var isDiskon = false;
	const isDesktopOrLaptop = useMediaQuery({
		query: "(min-device-width: 1224px)",
	});
	const isTablet = useMediaQuery({ query: "(max-width: 1224px)" });
	const isMobileDevice = useMediaQuery({
		query: "(max-device-width: 414px)",
	});

	const numberWithCommas = (x) => {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	};

	const addStuff = () => {
		setOrder(order + 1);
	};

	const addCart = () => {
		const orderList = {
			totalOrder: order,
			stuff: props.location.state,
		};
		users.addStuffToChartList(orderList);
		swal("Success", `You add ${props.location.state.name} to cart!`, "success");
		
	};

	if (props.location.state) {
		isDiskon = props.location.state.isDiskon;
		price = numberWithCommas(props.location.state.price);
		name = props.location.state.name;
		desc = props.location.state.description;
		type = props.location.state.type;
		priceDiskon = props.location.state.diskonPrice !== null ? numberWithCommas(props.location.state.diskonPrice) : null
	}

	if (isMobileDevice) {
		return (
			<div className="Pages">
				<div className="Detail">
					<div className="ContentDetails">
						<div className="ContentImg">
							<img src={example} alt="image" />
						</div>
						<div className="ContentText">
							<h1>{name}</h1>
							<span className="Type">{type}</span>
							{isDiskon ? (
								<div style={{ marginTop: "1rem" }}>
									<span className="Prices">
										Rp &nbsp;{priceDiskon}
									</span>
									<span className="PriceDisc">
										Rp &nbsp;{price}
									</span>
								</div>
							) : (
								<span className="Prices">Rp &nbsp;{price}</span>
							)}
							<span className="Textcontent">{desc}</span>
						</div>
						<div className="ContentPrice">
							<div className="Left">
								<FontAwesomeIcon icon={faMinusCircle} />
								<input type="text" value={order} />
								<FontAwesomeIcon
									icon={faPlusCircle}
									onClick={addStuff}
								/>
							</div>
							<div className="Right">
								<div className="Button" onClick={addCart}>
									<button>Add to cart</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<Footer></Footer>
			</div>
		);
	} else if (isDesktopOrLaptop) {
		return <></>;
	} else if (isTablet) {
		return <></>;
	}
}

export default ProductDetail;
