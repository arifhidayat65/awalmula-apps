import React, { useState } from "react";
import styles from "./Login.module.css";
import logo from "../../assets/icon2.png";
import * as users from "../../features/userAction";
import { useMediaQuery } from "react-responsive";

function Login(props) {
	const [error, setErrors] = useState({});
	const isDesktopOrLaptop = useMediaQuery({
		query: "(min-device-width: 1224px)",
	});
	const isTablet = useMediaQuery({ query: "(max-width: 1224px)" });
	const isMobileDevice = useMediaQuery({
		query: "(max-device-width: 414px)",
	});
	const [form, setForm] = useState({
		username: null,
		password: null,
	});

	const handleChange = (e) => {
		const { id, value } = e.target;
		setForm((prevState) => ({
			...prevState,
			[id]: value,
		}));
	};

	const validationInput = (form) => {
		const errorMsg = {};
		errorMsg.username = !form.username ? "Please input username" : "";
		errorMsg.password = !form.password ? "Please input password" : "";

		setErrors(errorMsg);

		for (let key in errorMsg) {
			if (errorMsg[key]) {
				return false;
			}
		}
		return true;
	};

	const handleSubmit = () => {
		if (validationInput(form)) {
			users.setUser(form);
			window.location.assign("/");
		} else {
			console.log("Error Login");
		}
	};

	if (isMobileDevice) {
		return (
			<div className="Pages">
				<div className={styles.Page}>
					<div className={styles.ImageBackground}>
						<img src={logo} alt="logo" />
					</div>
					<div className={styles.LoginForm}>
						<div className={styles.Form}>
							<div className={styles.Input}>
								<input
									placeholder="Username"
									type="text"
									id="username"
									value={form.username}
									onChange={handleChange}
								/>
								{error.username && (
									<span
										style={{
											color: "#b70e0e",
											fontSize: "12px",
										}}
									>
										{error.username}
									</span>
								)}
							</div>
							<div className={styles.Input}>
								<input
									placeholder="Password"
									type="password"
									value={form.password}
									onChange={handleChange}
									id="password"
								/>
								{error.password && (
									<span
										style={{
											color: "#b70e0e",
											fontSize: "12px",
										}}
									>
										{error.password}
									</span>
								)}
							</div>
							<button onClick={handleSubmit}>
								<span>LOGIN</span>
							</button>

							<div className={styles.Bottom}>
								<span>
									Don't have an account? <b>SIGN UP</b>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	} else if (isDesktopOrLaptop) {
		return <></>;
	} else if (isTablet) {
		return <></>;
	}
}

export default Login;
