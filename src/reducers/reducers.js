import { combineReducers } from "redux";
import users from "../features/userSlice";
import products from "../features/productSlice";

export default combineReducers({
	users,
	products
});
