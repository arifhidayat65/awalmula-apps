import React from "react";
import { Redirect, Route } from "react-router-dom";
import store from "../Store";

const PrivateRoute = (props) => {
	const { users } = store.getState();
	const { component: Component, ...rest } = props;

	return (
		<Route
			{...rest}
			render={() => {
				if (users.username && users.password) {
					return <Component {...rest} />;
				} else {
					return <Redirect to="/login" />;
				}
			}}
		/>
	);
};

export default PrivateRoute;
