import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faHome,
	faUserCircle,
	faShoppingCart,
} from "@fortawesome/free-solid-svg-icons";
import UseScrollPosition from "@react-hook/window-scroll";
import { Link } from "react-router-dom";

function Footer(props) {
	const ScrollY = UseScrollPosition(0);
	let sticky;
	let pathName = window.location.pathname;
	const [paths] = useState([
		{
			url: "/cart",
			isActive: false,
			icon: faShoppingCart,
		},
		{
			url: "/",
			isActive: false,
			icon: faHome,
		},
		{
			url: "/account",
			isActive: false,
			icon: faUserCircle,
		},
	]);

	if (ScrollY >= "0") {
		sticky = "Sticky";
	} else {
		sticky = "Unsticky";
	}

	const isDesktopOrLaptop = useMediaQuery({
		query: "(min-device-width: 1224px)",
	});
	const isTablet = useMediaQuery({ query: "(max-width: 1224px)" });
	const isMobileDevice = useMediaQuery({
		query: "(max-device-width: 414px)",
	});

	if (isMobileDevice) {
		return (
			<div className={["Footer", sticky].join(" ")}>
				<div className="Child">
					{paths.map((path, i) => {
						let Actived;

						if (pathName === path.url) {
							Actived = "Actived";
						} else {
							Actived = "";
						}
						return (
							<Link to={path.url} key={`path-${i}`}>
								<div className={["Items", Actived].join(" ")}>
									<FontAwesomeIcon icon={path.icon} />
								</div>
							</Link>
						);
					})}
				</div>
			</div>
		);
	} else if (isDesktopOrLaptop) {
		return <></>;
	} else if (isTablet) {
		return <></>;
	}
}

export default Footer;
