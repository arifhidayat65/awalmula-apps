import React from "react";
import { useMediaQuery } from "react-responsive";

function Header(props) {
	const isDesktopOrLaptop = useMediaQuery({
		query: "(min-device-width: 1224px)",
	});
	const isTablet = useMediaQuery({ query: "(max-width: 1224px)" });
	const isMobileDevice = useMediaQuery({
		query: "(max-device-width: 414px)",
	});

	if (isMobileDevice) {
		return <div className="Header"></div>;
	} else if (isDesktopOrLaptop) {
		return <></>;
	} else if (isTablet) {
		return <></>;
	}
}

export default Header;
